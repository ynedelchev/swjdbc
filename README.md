Failover JDBC Wrapper
=====================

<!-- Please use markdown-toc -i README.md to update the table of contents -->
<!-- -->
**Table of Contents**  *generated with [Markdown-TOC](https://www.npmjs.com/package/markdown-toc#install)*

<!-- toc -->

- [Overview](#overview)
- [Problem](#problem)
  * [Extensions](#extensions)
- [How it works](#how-it-works)
    + [Using environment variable to define the actual JDBC driver](#using-environment-variable-to-define-the-actual-jdbc-driver)
    + [Using java system property to define the actual JDBC driver](#using-java-system-property-to-define-the-actual-jdbc-driver)
- [Failover Username or Database URL](#failover-username-or-database-url)
  * [Failover username](#failover-username)
  * [Failover Database URL](#failover-database-url)
- [License](#license)

<!-- tocstop -->

Overview
========
This is a wrapper around a JDBC (Java Database Connectivity) driver that could use multiple connection strings, multiple users and multiple passwords.
If the first combination of url, username, password fails, then it will continue with the next one and so on untill it finds a valid on.
If no valid url, username, passord combination is found then it will fail with a reasonable error message. 

Problem
=======
The problem this wrapper is trying to solve is the following:

Imagine that you have a service which is using a database connection to store its data and has really high availability and fault tollerance. 
If it fails, then you just restart it and it need to continue to work. 
That is usually the case with micro-services.
````
        +---------+
        | Service |
        +---------+
             |
             | 
             |
             V
    _.-^^---....,,--
 --                  --
<                        >)
|          DB             |
|\._                   _./|
|   ```--. . , ; .--'''   |
|                         |
 \._                   _./ 
    ```--. . , ; .--'''    
    
````
    

Now imagine that you also need to periodically change the username or database password that this service is using to connect to the database. 
In general rotating the database passwords is usually a good practice.

So obviously after you change the database password, you need to also change the service configuration so that next time the service is restarted
it correctly takes the new configuration. 

However what will happen if the service is restarted just after you have changed the database configuration, but before updating the service configuration. 
In that case the service will simply fail to start since it would take the old configuration which is not yet valid. 

To avoid that we have created this JDBC driver wrapper which is capable of trying a password and if it fails, then it tries a failover password.

Thus your flow of changing the database password should be:

  1. Update service configuration to contain oldpassword;newpassword
  2. Chgange the actual database password to newpassword. 
  3. Update service configuration to contain newpassword;newpassword

Extensions
----------
Although this JDBC wrapper has been created to solve the above use case, it has been generalized to support multiple passwords and multiple usernames even multiple connection urls. So you could use it also in cases when you need to change the username or even the connection url (lets say, you want to point it to a different database instance). Or you could also use it it when you need several failover capabilities. 

How it works
============
In most cases your Java database connectivity code would look like this:

````java 
import java.sql.*;
...
   // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/TBL";

   //  Database credentials
   static final String USER = "username";
   static final String PASS = "password";
...
   Class.forName(JDBC_DRIVER);

   conn = DriverManager.getConnection(DB_URL,USER,PASS);

   stmt = conn.createStatement();
   String sql = "SELECT id, first, last, age FROM Employees";
   ResultSet rs = stmt.executeQuery(sql);
````
As seen from the example, we are using a single set of credentials (single username and a single password) as well as a fixed connection URL.

If we need to use a fallback value of any of the above properties (connection URL, username, password), then you just use list all of them in a colon separated list.
For example 

````java
   static final String PASS = "password";
````
would become
````java
   static final String PASS = "oldpassword;newpassword";
````
If you need to use a colon (`;`) somewhere inside the value, you need to escape it with a backslash in front, just like this (`\;`).
If you need to use a backslash (`\`), then you need to escape it as well, like this (`\\`).
In general a backslash would escape any symbol that fallows it. 

Next thing, that you need to do is to use our Wrapper JDBC driver () instead of the actual one.
So change this:
````java
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
````
into
````java
   static final String JDBC_DRIVER = "org.smoothcode.jdbc.failover.WrapperDriver";
````
At the end you need to tell the wrapper driver which actual JDBC driver to use.
You can do that in two ways:

### Using environment variable to define the actual JDBC driver
You can do that by exporting the environment variable `JDBC_DRIVER`. Example for bash shell would be:

   `$ export JDBC_DRIVER="com.mysql.jdbc.Driver"`

### Using java system property to define the actual JDBC driver
You can do that by specifying the java system property on the command line when you start the service via java:
````
   java -DJDBC_DRIVER=com.mysql.jdbc.Driver ....
````

or you could also do that programatically in your java code like this: 
````java 
    System.setProperty("JDBC_DRIVER", "com.mysql.jdbc.Driver");
````
   
at the end your Java program would end up looking similar to that: 

````java
import java.sql.*;
...
   // Specify the wrapper JDBC driver.
   static final String JDBC_DRIVER = "org.smoothcode.jdbc.failover.WrapperDriver"; 
   
   // Database URL
   static final String DB_URL = "jdbc:mysql://localhost/TBL";

   //  Database credentials
   static final String USER = "username";
   static final String PASS = "oldpassword;newpassword"; // Both passwords have been specified.
...
   // Specify the actual JDBC driver that the Wrapper driver would really use.
   System.setProperty("JDBC_DRIVER", "com.mysql.jdbc.Driver");
   
   // Load the wrapper driver, which behind the scenes would try to load the actual JDBC driver 
   // thrying both passwords one after the other untill one succeeds. 
   Class.forName(JDBC_DRIVER);

   conn = DriverManager.getConnection(DB_URL,USER,PASS);

   stmt = conn.createStatement();
   String sql = "SELECT id, first, last, age FROM Employees";
   ResultSet rs = stmt.executeQuery(sql);
````

Failover Username or Database URL
=================================

The same mechanism exists also for usernames and even database URL. 
Just use the same syntaxis with separating possible failover usernames or database URLS with semicolon.
Examples:

Failover username
-----------------

````java
    static final String USER = "oldusername;newusername";
````

Failover Database URL
---------------------

````java
    static final String DB_URL = "jdbc:mysql://oldhost/TBL;jdbc:mysql://newhost/TBL";
````


````
                       +---------+
                       | Service |
                       +---------+
                           /\
                  ________/  \________
                 /                    \
 First try this /                Second\try this 
 mysql:/oldhost/TBL              mysql:/newhost/TBL
              /                          \
             V                            V
    _.-^^---....,,--             _.-^^---....,,--
 --                  --       --                  --
<                        >)  <                        >)
|          DB 1  (old)    |  |          DB 2   (new)   |
|\._                   _./|  |\._                   _./|
|   ```--. . , ; .--'''   |  |   ```--. . , ; .--'''   |
|                         |  |                         |
 \._                   _./    \._                   _./ 
    ```--. . , ; .--'''          ```--. . , ; .--'''    
    
````

if you need to escape the semicolon symbol (`;`), then just put a backslash in front of it (`\;`).
If you need to escape the the backslash symbol (`\`), then just double it (`\\`).

If you have more than one failover factors, then the wrapper JDBC driver will try any possible combination that makes sense.




Thus you can make your 


License
=======

[MIT License](http://adampritchard.mit-license.org/).
