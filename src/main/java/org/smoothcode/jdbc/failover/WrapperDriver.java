package org.smoothcode.jdbc.failover;

import java.sql.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.Enumeration;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;


public class WrapperDriver implements Driver {

  static {
    try {
      DriverManager.registerDriver(new WrapperDriver());
    } catch (SQLException e) {
      throw new RuntimeException("Can't register wrapper JDBC driver!", e);
    }
  }	

  private Driver driver;

  public WrapperDriver() throws SQLException {
    String actualDriverClass = System.getenv("JDBC_DRIVER");
    if (actualDriverClass == null) {
      actualDriverClass = System.getProperty("JDBC_DRIVER");
    }
    if (actualDriverClass == null) {
      throw new SQLException("This is a failover wrapper around JDBC driver, "
        + "but an actual JDBC driver cannot be found. Please specify the full "
	+ "class name of the actual JDBC driver either with system environment "
	+ "variable or with a Java System property named \"JDBC_DRIVER\". For "
	+ "example to set it as system environment variable use command like "
	+ "'export JDBC_DRIVER=com.mysql.jdbc.Driver'. To set a Java System "
	+ "Property use 'java -DJDBC_DRIVER=com.mysql.jdbc.Driver ...' when "
	+ "starting the java virtual machine.");
    }
    Class<?> actualClass = null;
    try {
      actualClass = Class.forName(actualDriverClass);
    } catch (ClassNotFoundException cnfe) {
      throw new SQLException("This is a JDBC wrapper around the actual JDBC "
        + "Driver with class \"" + actualDriverClass + "\", but this class "
	+ "cannot be found in the class path (" + cnfe.getMessage() + "). "
	+ "Please make sure that the jar that contains it is in the class path.", cnfe);
    }
    // Here the actual driver should have already registered itself with the driver manager.
    // Lets find it.
    List<Driver> toBeUnregistered = new ArrayList<Driver>(1);
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    if (drivers != null) {
      while (drivers.hasMoreElements()) {
        Driver registeredDriver = drivers.nextElement();
	if (registeredDriver == null) {
	  continue;
	}
	String className = registeredDriver.getClass().getName();
	if (className != null && className.equals(actualDriverClass)) {
	  // We found it.
	  this.driver = registeredDriver;
	  toBeUnregistered.add(registeredDriver);
	}
      }
    }
    if (this.driver == null) {
      throw new SQLException("This is a JDBC failover wrapper driver over "
       + "the actual driver with class \"" + actualDriverClass 
       + "\", but this actual JDBC driver, does not seem to correctly register itself with"
       + " the driver manager.");
    }
    for (Driver registeredDriver : toBeUnregistered) {
      try {
        DriverManager.deregisterDriver(registeredDriver);
      } catch (SQLException sql) {
        throw new SQLException("", sql);     // TODO: 
      } catch (SecurityException se) {
        throw new SecurityException("", se); // TODO: 
      }
    }
  }

  public boolean acceptsURL(String url) throws SQLException {
    if (this.driver == null) {
      return false;
    }
    List<String> urls = splitUrl(url);
    if (urls == null) {
      return false;
    }
    for (String aurl : urls) {
      boolean canHandle = this.driver.acceptsURL(aurl);
      if (!canHandle) {
        return false;
      }
    }
    return true;
  }

  public Connection connect(String url, Properties info) throws SQLException {
    List<String> urls = splitUrl(url);
    List<String> users  = null;
    List<String> pases  = null;
    if (info != null) {
      users = splitUrl(info.getProperty("user"));
      pases = splitUrl(info.getProperty("password"));
      if (users != null && users.size() > 1 && pases !=null && pases.size() != users.size()) {
        throw new SQLException("There are " + users.size() + " different usernames to try and " 
	  + pases.size() + " different passwords to try, however for the user names it is "
	  + "allowed to either (a) not have any, (b) have just one user name or (c) the number "
	  + "of user names specified equals the number of the passwords specified (" 
	  + pases.size() + "). Once that condition is broken we do not know which password "
	  + "belongs to which user and do not know how to attempt a connection. Specified users: " 
	  + join(users)); 
      }
    }
    List<SQLException> exceptions = new ArrayList<SQLException>(2);
    Connection connection = null;
    for (String aurl : urls) {
       Properties props = info;
       if (pases != null) {
         props = new Properties(info);
	 if (pases != null) {
	    for (int i = 0; i < pases.size(); i++) {
	      String pass = pases.get(i);
	      props.setProperty("password", pass);
	      if (users != null && users.size() > 0) {
		 String user = null;
	         if (users.size() >= pases.size()) {
		   user = users.get(i);
		 } else {
		   user = users.get(0); 
		 }
		 props.setProperty("user", user);
	      }
	    }
	 }
       } 
       try {
         connection = this.driver.connect(aurl, props);
       } catch (SQLException sql) {
         exceptions.add(sql);
       }
       if (connection != null) {
         return connection;
       }
    }
    if (exceptions != null && exceptions.size() > 0) {
       throw new SQLException("Cannot connect after trying " + urls.size() 
	 + " different connection strings with " + ( pases == null ? 1 : pases.size() ) 
	 + " different credentials (passwords). "
	 + "Only the error for the first connection is reported. Connection Strings to try: " 
	 + join(urls) + "; Users to try: " + join(users) + "; Number of different passords to try: " 
	 + pases.size() + ".", exceptions.get(0));
    }
    return null;
  }

  public int getMajorVersion() {
    if (this.driver == null) {
      return 0;
    }
    return this.driver.getMajorVersion();
  }

  public int getMinorVersion() {
    if (this.driver == null) {
      return 0;
    }
    return this.driver.getMinorVersion();
  }

  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    if (this.driver == null) {
      return Logger.getGlobal();
    }
    return this.driver.getParentLogger();
  }

  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    return this.driver.getPropertyInfo(url, info);
  }
        
  public boolean jdbcCompliant() {
    if (this.driver == null) {
      return false;
    }
    return this.driver.jdbcCompliant();
  }

  private List<String> splitUrl(String url) {
    if (url == null) {
      return new ArrayList<String>(0);
    }
    List<String> urls = new ArrayList<String>(2);
    StringBuffer sb = new StringBuffer();
    boolean escaping = false;
    for (int i =0; i < url.length(); i++) {
      char chr = url.charAt(i);
      if (escaping) {
        escaping = false;
	sb.append(chr);
      } else if (chr == '\\') {
	  escaping = true;
      } else if (chr == ';') {
	String u = sb.toString();
	if (u != null) {
	  urls.add(u);
	}
	sb = new StringBuffer();
      } else {
        sb.append(chr);
      }
    }
    String u = sb.toString();
    if (u != null) {
      urls.add(u);
    }
    return urls;
  }

  private String join(List<String> list) {
     if (list == null) {
       return "";
     }
     StringBuffer sb = new StringBuffer();
     Iterator<String> iterator = list.iterator();
     while (iterator.hasNext()) {
       String str = iterator.next();
       sb.append("\"");
       sb.append(str);
       sb.append("\"");
       if (iterator.hasNext()) {
         sb.append(", ");
       }
     }
     return sb.toString();
  }

}
