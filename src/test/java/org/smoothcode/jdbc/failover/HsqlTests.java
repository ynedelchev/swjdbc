package org.smoothcode.jdbc.failover;

import org.hsqldb.server.Server;
import org.junit.Test;
import org.junit.BeforeClass;
import java.sql.*;
import java.net.InetAddress;

public class HsqlTests extends BasicTests {


  public HsqlTests() {
    super();
  }

  protected String getJdbcDriver() {
     return DRIVER;
  }

  protected String getJdbcUrl() {
     return URL;
  }

  protected String getFakeUrl() {
      return "jdbc:hsqldb:hsql://nonexistentserver:9001/mainDb";
  }

  protected String getJdbcUser() {
     return USER;
  }

  protected String getJdbcPassword() {
     return PASS;
  }

  private static final String USER = "SA";
  private static final String PASS = "";
  private static final String DRIVER = "org.hsqldb.jdbc.JDBCDriver";
  private static String URL = getUrl();
  static {
     
  }

  private static String getUrl() {
    try {
      InetAddress localhost = InetAddress.getLocalHost();
      return "jdbc:hsqldb:hsql://" + localhost.getHostAddress() + ":9001/mainDb";
    } catch (Throwable t) {
    }
    return null;
  }

  @BeforeClass
  public static void prepareDatabase() {
    if (URL == null) {
      URL = getUrl();
    }
    Server server = new Server();
    server.setDatabaseName(0, "mainDb");
    server.setDatabasePath(0, "mem:mainDb");
    server.setDatabaseName(1, "standbyDb");
    server.setDatabasePath(1, "mem:standbyDb");
    server.setPort(9001); // this is the default port
    server.start();

    
    try {
      Class.forName(DRIVER);
      Connection conn = DriverManager.getConnection(URL, USER, PASS);
      String sql = "CREATE TABLE test ( id INT, data VARCHAR(100));";
      CallableStatement statement = conn.prepareCall(sql);
      int result = statement.executeUpdate();
      if (result != 0) {
         throw new RuntimeException("Error while trying to prepare an in-memory database for the test to use. The in-memory database seems to be OK, but we cannot create a test table inside. The table creation statement returned " + result +  ", but 0 was expected. Sql statement was: " + sql);
      }
    } catch (Throwable t) {
    }
  }

}
