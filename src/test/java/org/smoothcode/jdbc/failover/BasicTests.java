package org.smoothcode.jdbc.failover;

import org.junit.Test;
import java.sql.*;

public abstract class BasicTests {

  private String JDBC_FAILOVER = "org.smoothcode.jdbc.failover.WrapperDriver";
  private String JDBC_DRIVER;
  private String DB_URL = "jdbc:mysql://localhost/test";

    //  Database credentials
  private String USER = null;
  private String PASS = null;

  public BasicTests() {
    DB_URL = getJdbcUrl();
    JDBC_DRIVER = getJdbcDriver(); 
    USER = getJdbcUser();
    PASS = getJdbcPassword();
  }

  protected abstract String getJdbcDriver();
  protected abstract String getJdbcUrl();
  protected abstract String getJdbcUser();
  protected abstract String getJdbcPassword();
  protected abstract String getFakeUrl();

  @Test
  public void testDbNormal() throws Exception {
    try {
      testConnection(JDBC_DRIVER, DB_URL, USER, PASS);
    } catch (Exception e) {
      throw new Exception("This test assumes that there is a running database server on the local host standard ports and it accepts connections from user \"" + USER + "\" with password \"" + PASS + "\", so if it fails one of the reasons could be that there is no such database server running on localhost or no such user/password configured for that server. Also it assumes that there is a table named \"test\". Actual failure details are : " + e.getClass().getName() + " : " + e.getLocalizedMessage());
    }
  }

  @Test
  public void testFailoverInPassword() throws Exception {
    try {
      System.setProperty("JDBC_DRIVER", JDBC_DRIVER);
      testConnection(JDBC_FAILOVER, "jdbc:mysql://nonexistentserver/test;" + DB_URL, USER, PASS);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testFailoverInUrlAndPassword() throws Exception {
    try {
      System.setProperty("JDBC_DRIVER", JDBC_DRIVER);
      String fakeUrl = getFakeUrl();
      testConnection(JDBC_FAILOVER, fakeUrl + ";" + DB_URL, USER, "fake;" + PASS);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testFailoverInUrlAndUserAndPassword() throws Exception {
    try {
      System.setProperty("JDBC_DRIVER", JDBC_DRIVER);
      String fakeUrl = getFakeUrl();
      testConnection(JDBC_FAILOVER, fakeUrl + ";" + DB_URL, "fake;" + USER, "fake;" + PASS);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testFailoverInUsernameAndPassword() throws Exception {
    try {
      System.setProperty("JDBC_DRIVER", JDBC_DRIVER);
      testConnection(JDBC_FAILOVER, DB_URL, "fake;" + USER, "fake;" + PASS);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testFailoverInUrl() throws Exception {
    try {
      System.setProperty("JDBC_DRIVER", JDBC_DRIVER);
      testConnection(JDBC_FAILOVER, DB_URL, USER, PASS);
    } catch (Exception e) {
      throw e;
    }
  }

  

  private void testConnection(String driver, String url, String user, String pass) throws Exception {
    // JDBC driver name and database URL
    Connection conn = null;
    Statement stmt = null;
    try{
      //STEP 2: Register JDBC driver
      Class.forName(driver);

      //STEP 3: Open a connection
      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(url,user,pass);

      //STEP 4: Execute a query
      System.out.println("Creating statement...");
      stmt = conn.createStatement();
      String sql;
      sql = "CREATE TABLE test ( id INT, data VARCHAR(100));";
      sql = "SELECT id from test;";
      ResultSet rs = stmt.executeQuery(sql);

      //STEP 5: Extract data from result set
      System.out.println("Starting result set.");
      while(rs.next()){
         //Retrieve by column name
         int id  = rs.getInt("id");

         //Display values
         System.out.print("ID: " + id);
      }
      System.out.println("End result set.");
      //STEP 6: Clean-up environment
      rs.close();
      stmt.close();
      conn.close();
    }catch(SQLException se){
      //Handle errors for JDBC
      se.printStackTrace();
      throw se;
    }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
      throw e;
    }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      }catch(SQLException se2){
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
    }//end try
  }


  @Test
  public void contextLoads() {
  }

}
