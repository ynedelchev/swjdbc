package org.smoothcode.jdbc.failover;

import org.junit.Test;
import org.junit.Ignore;
import java.sql.*;

@Ignore
public class OracleTests extends BasicTests {

  public OracleTests() {
    super();
  }

  protected String getJdbcDriver() {
     return "oracle.jdbc.OracleDriver";
  }

  protected String getJdbcUrl() {
     return "jdbc:oracle:thin:@localhost:1521:xe";
  }

  protected String getFakeUrl() {
     return "jdbc:oracle:thin:@nonexistentserver:1521:nonexistent";
  }

  protected String getJdbcUser() {
     return "system";
  }

  protected String getJdbcPassword() {
     return "oracle";
  }


}
