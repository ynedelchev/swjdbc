package org.smoothcode.jdbc.failover;

import org.junit.Test;
import org.junit.Ignore;
import java.sql.*;

@Ignore
public class MysqlTests extends BasicTests {


  public MysqlTests() {
    super();
  }

  protected String getJdbcDriver() {
     return "com.mysql.jdbc.Driver";
  }

  protected String getJdbcUrl() {
     return "jdbc:mysql://localhost/test";
  }

  protected String getFakeUrl() {
     return "jdbc:mysql://nonexistentserver/nonexistentdb";
  }

  protected String getJdbcUser() {
     return "root";
  }

  protected String getJdbcPassword() {
     return "root";
  }

}
